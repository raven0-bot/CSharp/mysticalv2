# MysticalV2

## Open Sourced Executor

- Level 7 Executor
- OxygenU DLL

## Issues & Suggestions

Send them [here](https://gitlab.com/raven0-bot/CSharp/mysticalv2/-/issues)

## Contributing / Merge Requests

Make them [here](https://gitlab.com/raven0-bot/CSharp/mysticalv2/-/merge_requests)
