﻿using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Diagnostics;
using System.Windows.Media.Animation;
using Microsoft.Win32;
using System.Threading;

namespace MysticalV2
{
    /// <summary>
    /// Interaction logic for Executor.xaml
    /// </summary>
    public partial class Executor : Window
    {
        Storyboard StoryBoard = new Storyboard();
        TimeSpan duration = TimeSpan.FromMilliseconds(500);
        TimeSpan duration2 = TimeSpan.FromMilliseconds(1000);





        private IEasingFunction Smooth
        {
            get;
            set;
        }
       = new QuarticEase
       {
           EasingMode = EasingMode.EaseInOut
       };

        public void Fade(DependencyObject Object)
        {
            DoubleAnimation FadeIn = new DoubleAnimation()
            {
                From = 0.0,
                To = 1.0,
                Duration = new Duration(duration),
            };
            Storyboard.SetTarget(FadeIn, Object);
            Storyboard.SetTargetProperty(FadeIn, new PropertyPath("Opacity", 1));
            StoryBoard.Children.Add(FadeIn);
            StoryBoard.Begin();
        }


        public void FadeOut(DependencyObject Object)
        {
            DoubleAnimation Fade = new DoubleAnimation()
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(duration),
            };
            Storyboard.SetTarget(Fade, Object);
            Storyboard.SetTargetProperty(Fade, new PropertyPath("Opacity", 1));
            StoryBoard.Children.Add(Fade);
            StoryBoard.Begin();
        }

        public void ObjectShift(DependencyObject Object, Thickness Get, Thickness Set)
        {
            ThicknessAnimation Animation = new ThicknessAnimation()
            {
                From = Get,
                To = Set,
                Duration = duration2,
                EasingFunction = Smooth,
            };
            Storyboard.SetTarget(Animation, Object);
            Storyboard.SetTargetProperty(Animation, new PropertyPath(MarginProperty));
            StoryBoard.Children.Add(Animation);
            StoryBoard.Begin();
        }

        OxygenUI_API.API Oxygen = new OxygenUI_API.API();
        OxygenUI_API.EventEmitter OxyEvent = new OxygenUI_API.EventEmitter();

        public void ReloadScripts()
        {
            this.List.Items.Clear();
            foreach (FileInfo fileInfo in new DirectoryInfo("./Scripts").GetFiles("*.txt"))
            {
                this.List.Items.Add(fileInfo.Name);
            }
            foreach (FileInfo fileInfo2 in new DirectoryInfo("./Scripts").GetFiles("*.lua"))
            {
                this.List.Items.Add(fileInfo2.Name);
            }
        }

        FileSystemWatcher files = new FileSystemWatcher();
        public Executor()
        {
            InitializeComponent();

            if (!Oxygen.doesDllExist())
            {
                Oxygen.DownloadDll();
            }
            if (!Oxygen.doesInjectorExist())
            {
                Oxygen.DownloadInjector();
            }

            Settings.Visibility = Visibility.Hidden;

            Stream stream = File.OpenRead("./bin/lua.xshd");
            XmlTextReader reader = new XmlTextReader(stream);
            AvalonEditor.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);

            Stream xshd_stream = File.OpenRead(Environment.CurrentDirectory + @"\bin\" + "Lua.xshd");
            XmlTextReader xshd_reader = new XmlTextReader(xshd_stream);
            AvalonEditor.SyntaxHighlighting = HighlightingLoader.Load(xshd_reader, HighlightingManager.Instance);

            xshd_reader.Close();
            xshd_stream.Close();
            this.EditTabs.Loaded += delegate (object source, RoutedEventArgs e)
            {
                this.EditTabs.GetTemplateItem<Button>("AddTabButton").Click += delegate (object s, RoutedEventArgs f)
                {
                    this.MakeTab("", "New Tab");
                };

                TabItem ti = EditTabs.SelectedItem as TabItem;
                ti.GetTemplateItem<Button>("CloseButton").Visibility = Visibility.Hidden;
                ti.GetTemplateItem<Button>("CloseButton").Width = 0;
                ti.Header = "Main Tab";

                this.tabScroller = this.EditTabs.GetTemplateItem<ScrollViewer>("TabScrollViewer");
            };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            this.List.Items.Clear();
            foreach (FileInfo fileInfo in new DirectoryInfo("./Scripts").GetFiles("*.txt"))
            {
                this.List.Items.Add(fileInfo.Name);
            }
            foreach (FileInfo fileInfo2 in new DirectoryInfo("./Scripts").GetFiles("*.lua"))
            {
                this.List.Items.Add(fileInfo2.Name);
            }
        }

        private void List_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool flag = this.List.SelectedIndex != -1;//So if have 1 item you can click it lol
            if (flag)
            {
                GetCurrent().Text = File.ReadAllText("scripts\\" + this.List.SelectedItem.ToString());
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Lua Script (*.lua)|*.lua|Txt Scripts (*.txt)|*.txt",
                FilterIndex = 1,
                RestoreDirectory = true,
                Title = "Save Scripts",
            };
            // Launch OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = openFileDialog.ShowDialog();
            // Get the selected file name and display in a TextBox.
            // Load content of file in a TextBlock
            if (result == true)
            {
                GetCurrent().Text = File.ReadAllText(openFileDialog.FileName);
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog {
            Filter = "Lua Scripts (*.lua)|*.lua|Txt Scripts (*.txt)|*.txt",
            Title = "Save Scripts",
            FilterIndex = 1,
            RestoreDirectory = true,
        };
            // Launch OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = saveFileDialog.ShowDialog();
            // Get the selected file name and display in a TextBox.
            // Load content of file in a TextBlock
            if (result == true)
            {
                File.WriteAllText(saveFileDialog.FileName, GetCurrent().Text);
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            GetCurrent().Text = "";
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (!Oxygen.isRobloxOn())
            {
                MessageBox.Show("Roblox is not running!", "Mystical | V2 | Powered by OxygenU API | ERR!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Oxygen.isAttached())
            {
                MessageBox.Show("Already injected");
                return;
            }
            Oxygen.Inject();

            return;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            if (!Oxygen.isAttached())
            {
                MessageBox.Show("Inject First!", "Mystical | V2 | Powered by OxygenU API | ERR!!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Oxygen.Execute(GetCurrent().Text);
            return;
        }

        private async void Button_Click_10(object sender, RoutedEventArgs e)
        {
            if (Settings.Visibility == Visibility.Hidden)
            {
                Settings.Visibility = Visibility.Visible;
                await Task.Delay(450);
                ObjectShift(Settings, Settings.Margin, new Thickness(768, 43, 0, 0));
                ObjectShift(TopBord, TopBord.Margin, new Thickness(-2, 0, 0, 0));
            }
            else
            {
                ObjectShift(Settings, Settings.Margin, new Thickness(768, 452, 0, -382.4));
                ObjectShift(TopBord, TopBord.Margin, new Thickness(0, -504, -1.6, 0));
                await Task.Delay(450);
                Settings.Visibility = Visibility.Hidden;
            }
        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            this.Topmost = true;
        }

        private void CheckBox_Checked_2(object sender, RoutedEventArgs e)
        {
            this.Opacity = .7;
        }
        private void CheckBox_Unloaded(object sender, RoutedEventArgs e)
        {
            this.Topmost = false;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.Opacity = 1;
        }

        private void KillRoblox(object sender, RoutedEventArgs e)
        {
            Oxygen.KillRoblox();
        }

            private ScrollViewer tabScroller;
        private void ScrollTabs(object sender, MouseWheelEventArgs e)
        {

            this.tabScroller.ScrollToHorizontalOffset(this.tabScroller.HorizontalOffset + (double)(e.Delta / 10));
        }
        private void MoveTab(object sender, MouseEventArgs e)
        {
            TabItem tabItem = e.Source as TabItem;
            if (tabItem == null)
            {
                return;
            }
            if (Mouse.PrimaryDevice.LeftButton == MouseButtonState.Pressed)
            {
                if (VisualTreeHelper.HitTest(tabItem, Mouse.GetPosition(tabItem)).VisualHit is Button)
                {
                    return;
                }
                DragDrop.DoDragDrop(tabItem, tabItem, DragDropEffects.Move);
            }
        }
        private ICSharpCode.AvalonEdit.TextEditor current;

        public ICSharpCode.AvalonEdit.TextEditor GetCurrent()
        {
            if (this.EditTabs.Items.Count == 0)
            {
                return AvalonEditor;
            }
            else
            {
                return this.current = (this.EditTabs.SelectedContent as ICSharpCode.AvalonEdit.TextEditor);
            }
        }





        public ICSharpCode.AvalonEdit.TextEditor MakeEditor()
        {
            ICSharpCode.AvalonEdit.TextEditor textEditor = new ICSharpCode.AvalonEdit.TextEditor
            {
                ShowLineNumbers = true,
                Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(22, 22, 22)),
                Foreground = new SolidColorBrush((System.Windows.Media.Color.FromRgb(255, 255, 255))),
                Margin = new Thickness(2, 5, 7, -11),
                FontFamily = new System.Windows.Media.FontFamily("Consolas"),
                Style = (this.TryFindResource("TextEditorStyle1") as Style),
                HorizontalScrollBarVisibility = ScrollBarVisibility.Visible,
                VerticalScrollBarVisibility = ScrollBarVisibility.Visible
            };
            textEditor.Options.EnableEmailHyperlinks = false;
            textEditor.Options.EnableHyperlinks = false;
            textEditor.Options.AllowScrollBelowDocument = true;
            Stream xshd_stream = File.OpenRead(Environment.CurrentDirectory + @"\bin\" + "lua.xshd");
            XmlTextReader xshd_reader = new XmlTextReader(xshd_stream);
            textEditor.SyntaxHighlighting = HighlightingLoader.Load(xshd_reader, HighlightingManager.Instance);

            xshd_reader.Close();
            xshd_stream.Close();
            return textEditor;
        }
        public TabItem MakeTab(string text = "", string title = "Tab")
        {
            title = title + "";
            bool loaded = false;
            ICSharpCode.AvalonEdit.TextEditor textEditor = MakeEditor();
            textEditor.Text = text;
            TabItem tab = new TabItem
            {
                Content = textEditor,
                Style = (base.TryFindResource("Tab") as Style),
                AllowDrop = true,
                Header = title
            };
            tab.MouseWheel += this.ScrollTabs;
            tab.Loaded += delegate (object source, RoutedEventArgs e)
            {
                if (loaded)
                {
                    return;
                }
                this.tabScroller.ScrollToRightEnd();
                loaded = true;
            };
            tab.MouseDown += delegate (object sender, MouseButtonEventArgs e)
            {
                if (e.OriginalSource is Border)
                {
                    if (e.MiddleButton == MouseButtonState.Pressed)
                    {
                        this.EditTabs.Items.Remove(tab);
                        return;
                    }
                }
            };
            tab.Loaded += delegate (object s, RoutedEventArgs e)
            {
                tab.GetTemplateItem<Button>("CloseButton").Click += delegate (object r, RoutedEventArgs f)
                {
                    this.EditTabs.Items.Remove(tab);
                };

                this.tabScroller.ScrollToRightEnd();
                loaded = true;
            };

            tab.MouseMove += this.MoveTab;
            tab.Drop += this.DropTab;
            string oldHeader = title;
            this.EditTabs.SelectedIndex = this.EditTabs.Items.Add(tab);
            return tab;
        }
        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void Ellipse_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
        private void EditTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DropTab(object sender, DragEventArgs e)
        {
            TabItem tabItem = e.Source as TabItem;
            if (tabItem != null)
            {
                TabItem tabItem2 = e.Data.GetData(typeof(TabItem)) as TabItem;
                if (tabItem2 != null)
                {
                    if (!tabItem.Equals(tabItem2))
                    {
                        TabControl tabControl = tabItem.Parent as TabControl;
                        int insertIndex = tabControl.Items.IndexOf(tabItem2);
                        int num = tabControl.Items.IndexOf(tabItem);
                        tabControl.Items.Remove(tabItem2);
                        tabControl.Items.Insert(num, tabItem2);
                        tabControl.Items.Remove(tabItem);
                        tabControl.Items.Insert(insertIndex, tabItem);
                        tabControl.SelectedIndex = num;
                    }
                    return;
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var view = CollectionViewSource.GetDefaultView(List.Items);
            if (view != null)
                view.Filter = (o) => { return o.ToString().ToLower().Contains(Search.Text.ToLower()); };
        }

        private void FPSUnlock(object sender, RoutedEventArgs e)
        {
            Process.Start("rbxfpsunlocker.exe");
        }

        private void Reinstall(object sender, RoutedEventArgs e)
        {
                Oxygen.DownloadDll();
                Oxygen.DownloadInjector();
            MessageBox.Show("Reinstalled!", "Mystical | V2 | Powered by OxygenU API", MessageBoxButton.OK, MessageBoxImage.Information);
            return;
        }
    }
}
